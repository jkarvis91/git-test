package game;

public interface Game {
	public void powerOn();
	public void powerOff();
	public void volumeUp();
	public void volumeDown();	
}
