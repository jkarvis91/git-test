package game;

import org.springframework.stereotype.Component;

@Component("game")
public class DiabloGame implements Game {
	
	public DiabloGame() {
		System.out.println("===> DiabloGame 객체 생성");
	}
	public void powerOn() { 
		System.out.println("DiabloGame---- 전원 켠다"); 
	} 
	public void powerOff() { 
		System.out.println("DiabloGame---- 전원 끈다"); 
	} 

	/*
	 * public void volumeUp() { System.out.println("DiabloGame---- 소리 키운다"); } public void
	 * volumeDown() { System.out.println("DiabloGame---- 소리 줄인다"); }
	 */
	public void volumeUp() {
		
	}
	public void volumeDown() {
	}
		 
}
